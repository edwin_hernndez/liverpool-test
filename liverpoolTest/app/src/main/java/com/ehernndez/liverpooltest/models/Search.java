package com.ehernndez.liverpooltest.models;

public class Search {

    private String name;
    private String price;
    private String imageURL;

    public Search(String name, String price, String imageURL) {
        this.name = name;
        this.price = price;
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getImageURL() {
        return imageURL;
    }
}