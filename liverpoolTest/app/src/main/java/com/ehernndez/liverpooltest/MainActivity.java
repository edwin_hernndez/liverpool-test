package com.ehernndez.liverpooltest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.liverpooltest.adapter.ArticulosAdapter;
import com.ehernndez.liverpooltest.models.Search;
import com.ehernndez.liverpooltest.singleton.Singleton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    TextInputEditText edtxtSearch;
    ImageButton btnSearch;
    RecyclerView rvArticulos;
    ArticulosAdapter articulosAdapter;

    RequestQueue requestQueue;
    Singleton singleton;
    ProgressDialog progressDialog;

    List<Search> articulosList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        singleton = Singleton.getInstance(this);
        requestQueue = singleton.getmRequestQueue();

        edtxtSearch = findViewById(R.id.edtxt_search);
        btnSearch = findViewById(R.id.btn_search);
        rvArticulos = findViewById(R.id.rv_articulos);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxtSearch.getText().toString().equals("")) {
                    Snackbar.make(view, "Es necesario escribir una palabra para realizar la busqueda", Snackbar.LENGTH_SHORT).show();
                } else {
                    searchArticle(edtxtSearch.getText().toString());
                }
            }
        });

        rvArticulos.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        rvArticulos.setLayoutManager(linearLayoutManager);

    }

    private void searchArticle(String keySearch) {
        Log.e("key", getString(R.string.url_base) + keySearch + "&d3106047a194921c01969dfdec083925=json");
        StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, getString(R.string.url_base) + keySearch + "&d3106047a194921c01969dfdec083925=json", new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Log.e("response --", jsonObject.toString());

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("contents"));
                    Log.e("response --", jsonArray.toString());

                    JSONObject jsonObject1 = new JSONObject(String.valueOf(jsonArray.getJSONObject(0)));

                    JSONArray jsonArray1 = jsonObject1.getJSONArray("mainContent");
                    Log.e("legnt", String.valueOf(jsonArray1.length()));

                    JSONObject jsonObject2 = new JSONObject(String.valueOf(jsonArray1.getJSONObject(1)));

                    JSONArray jsonArray2 = jsonObject2.getJSONArray("contents");
                    Log.e("response 4", jsonArray2.toString());

                    JSONObject jsonObject3 = new JSONObject(String.valueOf(jsonArray2.getJSONObject(0)));

                    JSONArray jsonArray3 = jsonObject3.getJSONArray("records");

                    Log.e("response 5", jsonArray3.toString());

                    llenarArticulos(jsonArray3);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_espere));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void llenarArticulos(JSONArray jsonArray) {
        articulosList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String name_card = jsonObject.getString("productDisplayName");
                String price_card = jsonObject.getString("listPrice");
                String image = jsonObject.getString("sku.thumbnailImage");

                articulosList.add(new Search(name_card, price_card, image));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        articulosAdapter = new ArticulosAdapter(articulosList);
        rvArticulos.setAdapter(articulosAdapter);
    }
}