package com.ehernndez.liverpooltest.adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ehernndez.liverpooltest.R;
import com.ehernndez.liverpooltest.models.Search;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArticulosAdapter extends RecyclerView.Adapter<ArticulosAdapter.ArticulosViewHolder> {

    Context context;
    List<Search> articles;

    public ArticulosAdapter(List<Search> articulosList) {
        this.articles = articulosList;

    }

    @NonNull
    @Override
    public ArticulosAdapter.ArticulosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ArticulosViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticulosAdapter.ArticulosViewHolder holder, int position) {

        String name = articles.get(position).getName().replace("[", "").replace("]", "").replace("\"", "");
        holder.txtName.setText(name);
        String price = articles.get(position).getPrice().replace("[", "").replace("]", "").replace("\"", "");
        holder.txtPrice.setText(price);
        String imagereq = articles.get(position).getImageURL();
        String image = imagereq.replace("\\", "").replace("[", "").replace("]", "").replace("\"", "");
        Uri uri = Uri.parse(image);
        Log.e("imageUrl", image);
        Picasso.get().load(uri).into(holder.imgArticle);

        context = holder.context;

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class ArticulosViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;
        Context context;
        ImageView imgArticle;

        public ArticulosViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtPrice = itemView.findViewById(R.id.txt_price);
            imgArticle = itemView.findViewById(R.id.img_article);
            context = itemView.getContext();
        }
    }
}
